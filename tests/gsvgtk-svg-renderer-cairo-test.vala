/* -*- Mode: vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */
/*
 * gsvg-actor-circle-test.vala

 * Copyright (C) 2018 Daniel Espinosa <daniel.espinosa@pwmc.mx>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using GSvg;
using GSvgtk;
using Gtkt;
using GXml;

class GSvgTest.Suite : Object
{
  static int main (string[] args)
  {
    GLib.Intl.setlocale (GLib.LocaleCategory.ALL, "");
    GtkClutter.init (ref args);
    Test.init (ref args);
    Test.add_func ("/gsvgtk/render/lines",
    ()=>{
      GSvg.Document svg = new GSvg.GsDocument ();
      var s = svg.add_svg (null, null, "100mm", "100mm");
      var l = s.create_line ("0mm", "0mm", "100mm", "100mm");
      l.stroke = "black";
      l.stroke_width = "1mm";
      l.id = "line";
      s.lines.append (l);
      var win = new Gtkt.WindowTester ();
      var w = new GtkClutter.Embed () {
        width_request = 400,
        height_request = 400 };
      w.get_stage ().width = 400;
      w.get_stage ().height = 400;
      w.get_stage ().x = 0;
      w.get_stage ().y = 0;
      var img = new GSvgtk.ActorImageClutter ();
      img.svg = svg;
      assert (img.svg != null);
      img.renderer = new GSvgtk.SvgRendererCairo ();
      assert (img.renderer is SvgRendererCairo);
      assert (img.renderer.svg != null);
      w.get_stage ().add_child (img);
      win.widget = w;
      win.add_test ("GSVGtk.Render Test","Render a Line");
      win.add_test ("GSVGtk.Render Test","Render a Line - translate");
      win.initialize.connect (()=>{
        switch (win.current_ntest) {
        case 1:
          img.drawing_area.invalidate ();
        break;
        case 2:
          l.x2.base_val.value = 10.0;
          l.y2.base_val.value = 10.0;
          l.transform = new GsAnimatedTransformList ();
          l.transform.value = "translate(200 200)";
          img.drawing_area.invalidate ();
        break;
        }
      });
      win.check.connect (()=>{
      });
      win.waiting_for_event = ConfigTest.TEST_INTERACTIVE;
      win.show_all ();
      Gtk.main ();
    });
    Test.add_func ("/gsvgtk/render/circles",
    ()=>{
      GSvg.Document svg = new GSvg.GsDocument ();
      var s = svg.add_svg (null, null, "100mm", "100mm");
      var c = s.create_circle ("50mm", "50mm", "47mm");
      c.stroke = "black";
      c.stroke_width = "1mm";
      c.id = "circle";
      s.circles.append (c);
      var win = new Gtkt.WindowTester ();
      var w = new GtkClutter.Embed () {
        width_request = 400,
        height_request = 400 };
      w.get_stage ().width = 400;
      w.get_stage ().height = 400;
      w.get_stage ().x = 0;
      w.get_stage ().y = 0;
      win.widget = w;
      win.add_test ("GSVGtk.Render Test","Render a Circle");
      win.add_test ("GSVGtk.Render Test","Render a Circle no fill");
      win.add_test ("GSVGtk.Render Test","Render a Circle fill in red");
      win.add_test ("GSVGtk.Render Test","Render a Circle translated");
      var img = new GSvgtk.ActorImageClutter ();
      img.svg = svg;
      assert (img.svg != null);
      img.renderer = new GSvgtk.SvgRendererCairo ();
      assert (img.renderer is SvgRendererCairo);
      assert (img.renderer.svg != null);
      w.get_stage ().add_child (img);
      win.initialize.connect (()=>{
        switch (win.current_ntest) {
        case 1:
          img.drawing_area.invalidate ();
        break;
        case 2:
          c.fill = "none";
          img.drawing_area.invalidate ();
        break;
        case 3:
          c.fill = "red";
          img.drawing_area.invalidate ();
        break;
        case 4:
          c.transform = new GsAnimatedTransformList ();
          c.transform.value = "translate(100 100)";
          img.drawing_area.invalidate ();
        break;
        }
      });
      win.check.connect (()=>{
      });
      win.waiting_for_event = ConfigTest.TEST_INTERACTIVE;
      win.show_all ();
      Gtk.main ();
    });
    Test.add_func ("/gsvgtk/render/rectangles",
    ()=>{
      GSvg.Document svg = new GSvg.GsDocument ();
      var s = svg.add_svg (null, null, "100mm", "100mm");
      var r = s.create_rect ("3mm", "3mm", "47mm","47mm", null, null);
      r.stroke = "black";
      r.stroke_width = "1mm";
      r.id = "rect";
      s.rects.append (r);
      var win = new Gtkt.WindowTester ();
      var w = new GtkClutter.Embed () {
        width_request = 400,
        height_request = 400 };
      w.get_stage ().width = 400;
      w.get_stage ().height = 400;
      w.get_stage ().x = 0;
      w.get_stage ().y = 0;
      win.widget = w;
      var img = new GSvgtk.ActorImageClutter ();
      img.svg = svg;
      assert (img.svg != null);
      img.renderer = new GSvgtk.SvgRendererCairo ();
      assert (img.renderer is SvgRendererCairo);
      assert (img.renderer.svg != null);
      win.add_test ("GSVGtk.Render Test","Render a Rectangle default filled");
      win.add_test ("GSVGtk.Render Test","Render a Rectangle no fill");
      win.add_test ("GSVGtk.Render Test","Render a Rectangle fill in green");
      win.add_test ("GSVGtk.Render Test","Render a Rectangle fill in red rounded");
      win.add_test ("GSVGtk.Render Test","Render a Rectangle fill in red rounded rx 10mm ry 15mm");
      win.add_test ("GSVGtk.Render Test","Render a Rectangle fill in red rounded rx > width/2 ry > height/2");
      win.add_test ("GSVGtk.Render Test","Render a Rectangle fill in red rounded rx 18mm ry 10mm");
      win.add_test ("GSVGtk.Render Test","Render a Rectangle fill in fill none - green rounded stroke rx 7mm ry 7mm");
      win.add_test ("GSVGtk.Render Test","Render a Rectangle translated");
      win.initialize.connect (()=>{
        switch (win.current_ntest) {
        case 1:
          w.get_stage ().add_child (img);
        break;
        case 2:
          r.fill = "none";
          img.drawing_area.invalidate ();
        break;
        case 3:
          r.fill = "green";
          img.drawing_area.invalidate ();
        break;
        case 4:
          r.fill = "red";
          r.stroke = null;
          r.rx = new GsAnimatedLengthRX ();
          r.rx.value = "15mm";
          img.drawing_area.invalidate ();
        break;
        case 5:
          r.fill = "red";
          r.stroke = null;
          r.rx = new GsAnimatedLengthRX ();
          r.rx.value = "10mm";
          r.ry = new GsAnimatedLengthRY ();
          r.ry.value = "15mm";
          img.drawing_area.invalidate ();
        break;
        case 6:
          r.fill = "red";
          r.stroke = null;
          r.rx = new GsAnimatedLengthRX ();
          r.rx.value = "40mm";
          r.ry = new GsAnimatedLengthRY ();
          r.ry.value = "45mm";
          img.drawing_area.invalidate ();
        break;
        case 7:
          r.fill = "red";
          r.stroke = null;
          r.rx = new GsAnimatedLengthRX ();
          r.rx.value = "18mm";
          r.ry = new GsAnimatedLengthRY ();
          r.ry.value = "10mm";
          img.drawing_area.invalidate ();
        break;
        case 8:
          r.fill = "none";
          r.stroke = "green";
          r.stroke_width = "3mm";
          r.rx = new GsAnimatedLengthRX ();
          r.rx.value = "7mm";
          r.ry = new GsAnimatedLengthRY ();
          r.ry.value = "7mm";
          img.drawing_area.invalidate ();
        break;
        case 9:
          r.transform = new GsAnimatedTransformList ();
          r.transform.value = "translate(100 100)";
          img.drawing_area.invalidate ();
        break;
        }
      });
      win.check.connect (()=>{
      });
      win.waiting_for_event = ConfigTest.TEST_INTERACTIVE;
      win.show_all ();
      Gtk.main ();
    });
    Test.add_func ("/gsvgtk/render/text",
    ()=>{
      try {
        GSvg.Document svg = new GSvg.GsDocument ();
        var s = svg.add_svg (null, null, "100mm", "100mm");
        var t = s.create_text ("GSVGtk", "5mm", "50mm", null, null, null);
        t.fill = "black";
        t.font_family = "Verdana";
        t.font_size = "3mm";
        t.id = "5mm";
        s.texts.append (t);
        var l1 = s.create_line ("50mm", "0mm", "50mm", "100mm");
        l1.id = "guide1";
        l1.stroke = "black";
        l1.stroke_width = ".25mm";
        s.lines.append (l1);
        var l2 = s.create_line ("5mm", "50mm", "100mm", "50mm");
        l2.id = "guide2";
        l2.stroke = "black";
        l2.stroke_width = ".25mm";
        s.lines.append (l2);
        var l3 = s.create_line ("5mm", "0mm", "5mm", "100mm");
        l3.id = "guide3";
        l3.stroke = "black";
        l3.stroke_width = ".25mm";
        s.lines.append (l3);
        var win = new Gtkt.WindowTester ();
        var w = new GtkClutter.Embed () {
          width_request = 400,
          height_request = 400 };
        w.get_stage ().width = 400;
        w.get_stage ().height = 400;
        w.get_stage ().x = 0;
        w.get_stage ().y = 0;
        win.widget = w;
        var img = new GSvgtk.ActorImageClutter ();
        img.svg = svg;
        message (img.svg.write_string ());
        assert (img.svg != null);
        img.renderer = new GSvgtk.SvgRendererCairo ();
        assert (img.renderer is SvgRendererCairo);
        assert (img.renderer.svg != null);
        w.get_stage ().add_child (img);
        win.add_test ("GSVGtk.Render Test","Render a Text 'GSVGtk'");
        win.add_test ("GSVGtk.Render Test","Render a Text 'GSVGtk' in blue");
        win.add_test ("GSVGtk.Render Test","Render a Text 'GSVGtk' in blue, moved 5mm,10mm 10mm,45mm");
        win.add_test ("GSVGtk.Render Test","Render a Text 'GSVGtk' in blue, moved 5mm,10mm 10mm,45mm 15mm,25mm");
        win.add_test ("GSVGtk.Render Test","Render a Text 'GSVGtk' in blue, moved 5mm,10mm 10mm,45mm x,25mm");
        win.add_test ("GSVGtk.Render Test","Render a Text 'GSVGtk' in blue, moved 5mm,10mm 10mm,45mm 7mm,25mm");
        win.add_test ("GSVGtk.Render Test","Render a Text 'GSVGtk' italic");
        win.add_test ("GSVGtk.Render Test","Render a Text 'GSVGtk' oblique");
        win.add_test ("GSVGtk.Render Test","Render a Text 'GSVGtk' Liberation Sans");
        win.add_test ("GSVGtk.Render Test","Render a Text 'GSVGtk' Liberation Sans - Italic Bold");
        win.add_test ("GSVGtk.Render Test","Render a Text 'GSVGtk' Liberation Sans - middle");
        win.add_test ("GSVGtk.Render Test","Render a Text 'GSVGtk' Liberation Sans - end");
        win.add_test ("GSVGtk.Render Test","Render a Text 'GSVGtk' Liberation Sans - stroke in red");
        win.add_test ("GSVGtk.Render Test","Render a Text 'GSVGtk' translated");
        win.initialize.connect (()=>{
          switch (win.current_ntest) {
          case 1:
            img.drawing_area.invalidate ();
          break;
          case 2:
            t.fill = "blue";
            img.drawing_area.invalidate ();
          break;
          case 3:
            t.x = new GsAnimatedLengthList ();
            t.y = new GsAnimatedLengthList ();
            t.x.base_val.value = "5mm 10mm";
            t.y.base_val.value = "10mm 45mm";
            img.drawing_area.invalidate ();
          break;
          case 4:
            t.x = new GsAnimatedLengthList ();
            t.y = new GsAnimatedLengthList ();
            t.x.base_val.value = "5mm 10mm 15mm";
            t.y.base_val.value = "10mm 45mm 25mm";
            img.drawing_area.invalidate ();
          break;
          case 5:
            t.x = new GsAnimatedLengthList ();
            t.y = new GsAnimatedLengthList ();
            t.x.base_val.value = "5mm 10mm";
            t.y.base_val.value = "10mm 45mm 25mm";
            img.drawing_area.invalidate ();
          break;
          case 6:
            t.x = new GsAnimatedLengthList ();
            t.y = new GsAnimatedLengthList ();
            t.x.base_val.value = "5mm 10mm 7mm";
            t.y.base_val.value = "10mm 45mm 25mm";
            img.drawing_area.invalidate ();
          break;
          case 7:
            t.x = new GsAnimatedLengthList ();
            t.y = new GsAnimatedLengthList ();
            t.x.base_val.value = "5mm";
            t.y.base_val.value = "50mm";
            t.font_style = "italic";
            img.drawing_area.invalidate ();
          break;
          case 8:
            t.x = new GsAnimatedLengthList ();
            t.y = new GsAnimatedLengthList ();
            t.x.base_val.value = "5mm";
            t.y.base_val.value = "50mm";
            t.font_style = "oblique";
            img.drawing_area.invalidate ();
          break;
          case 9:
            t.x = new GsAnimatedLengthList ();
            t.y = new GsAnimatedLengthList ();
            t.x.base_val.value = "5mm";
            t.y.base_val.value = "50mm";
            t.font_style = null;
            t.font_weight = null;
            t.font_family = "Liberation Sans";
            img.drawing_area.invalidate ();
          break;
          case 10:
            t.x = new GsAnimatedLengthList ();
            t.y = new GsAnimatedLengthList ();
            t.x.base_val.value = "5mm";
            t.y.base_val.value = "50mm";
            t.font_style = "italic";
            t.font_weight = "bold";
            t.font_family = "Liberation Sans";
            img.drawing_area.invalidate ();
          break;
          case 11:
            t.x = new GsAnimatedLengthList ();
            t.y = new GsAnimatedLengthList ();
            t.x.base_val.value = "50mm";
            t.y.base_val.value = "50mm";
            t.font_style = "italic";
            t.font_weight = "bold";
            t.font_family = "Liberation Sans";
            t.text_anchor = "middle";
            img.drawing_area.invalidate ();
          break;
          case 12:
            t.x = new GsAnimatedLengthList ();
            t.y = new GsAnimatedLengthList ();
            t.x.base_val.value = "50mm";
            t.y.base_val.value = "50mm";
            t.font_style = "italic";
            t.font_weight = "bold";
            t.font_family = "Liberation Sans";
            t.text_anchor = "end";
            img.drawing_area.invalidate ();
          break;
          case 13:
            t.x = new GsAnimatedLengthList ();
            t.y = new GsAnimatedLengthList ();
            t.x.base_val.value = "50mm";
            t.y.base_val.value = "50mm";
            t.font_style = null;
            t.font_weight = "900";
            t.font_family = "Liberation Sans";
            t.text_anchor = "middle";
            t.stroke = "red";
            t.stroke_width = "0.2mm";
            img.drawing_area.invalidate ();
          break;
          case 14:
            t.transform = new GsAnimatedTransformList ();
            t.transform.value = "translate(100 100)";
            img.drawing_area.invalidate ();
          break;
          }
        });
        win.check.connect (()=>{
        });
        win.waiting_for_event = ConfigTest.TEST_INTERACTIVE;
        win.show_all ();
        Gtk.main ();
      } catch (GLib.Error e) {
        warning ("Error: %s", e.message);
      }
    });
    Test.add_func ("/gsvgtk/render/ellipse",
    ()=>{
      GSvg.Document svg = new GSvg.GsDocument ();
      var s = svg.add_svg (null, null, "100mm", "100mm");
      var e = s.create_ellipse ("50mm", "50mm", "47mm", "30mm");
      e.id = "ellipse";
      s.ellipses.append (e);
      var win = new Gtkt.WindowTester ();
      var w = new GtkClutter.Embed () {
        width_request = 400,
        height_request = 400 };
      w.get_stage ().width = 400;
      w.get_stage ().height = 400;
      w.get_stage ().x = 0;
      w.get_stage ().y = 0;
      win.widget = w;
      var img = new GSvgtk.ActorImageClutter ();
      img.svg = svg;
      assert (img.svg != null);
      img.renderer = new GSvgtk.SvgRendererCairo ();
      assert (img.renderer is SvgRendererCairo);
      assert (img.renderer.svg != null);
      w.get_stage ().add_child (img);
      win.add_test ("GSVGtk.Render Test","Render an Ellipse");
      win.add_test ("GSVGtk.Render Test","Render an Ellipse no fill");
      win.add_test ("GSVGtk.Render Test","Render an Ellipse fill, no stroke");
      win.add_test ("GSVGtk.Render Test","Render an Ellipse translated");
      win.initialize.connect (()=>{
        switch (win.current_ntest) {
        case 1:
          e.stroke = "red";
          e.stroke_width = "1mm";
          img.drawing_area.invalidate ();
        break;
        case 2:
          e.fill = "none";
          e.stroke = "blue";
          e.stroke_width = "1mm";
          img.drawing_area.invalidate ();
        break;
        case 3:
          e.fill = "green";
          e.stroke = null;
          e.stroke_width = null;
          img.drawing_area.invalidate ();
        break;
        case 4:
          e.transform = new GsAnimatedTransformList ();
          e.transform.value = "translate(100 100)";
          img.drawing_area.invalidate ();
        break;
        }
      });
      win.check.connect (()=>{
      });
      win.waiting_for_event = ConfigTest.TEST_INTERACTIVE;
      win.show_all ();
      Gtk.main ();
    });
    Test.add_func ("/gsvgtk/render/polyline",
    ()=>{
      GSvg.Document svg = new GSvg.GsDocument ();
      var s = svg.add_svg (null, null, "100mm", "100mm");
      var pl = s.create_polyline ("100,100 150,100 150,50 200,50 200,100 250,100");
      pl.id = "pòlyline";
      s.polylines.append (pl);
      var win = new Gtkt.WindowTester ();
      var w = new GtkClutter.Embed () {
        width_request = 400,
        height_request = 400 };
      w.get_stage ().width = 400;
      w.get_stage ().height = 400;
      w.get_stage ().x = 0;
      w.get_stage ().y = 0;
      win.widget = w;
      var img = new GSvgtk.ActorImageClutter ();
      img.svg = svg;
      assert (img.svg != null);
      img.renderer = new GSvgtk.SvgRendererCairo ();
      assert (img.renderer is SvgRendererCairo);
      assert (img.renderer.svg != null);
      w.get_stage ().add_child (img);
      win.add_test ("GSVGtk.Render Test","Render a Polyline");
      win.add_test ("GSVGtk.Render Test","Render a Polyline translated");
      win.initialize.connect (()=>{
        switch (win.current_ntest) {
        case 1:
          pl.stroke = "red";
          pl.stroke_width = "1mm";
          img.drawing_area.invalidate ();
        break;
        case 2:
          pl.transform = new GsAnimatedTransformList ();
          pl.transform.value = "translate(100 100)";
          img.drawing_area.invalidate ();
        break;
        }
      });
      win.check.connect (()=>{
      });
      win.waiting_for_event = ConfigTest.TEST_INTERACTIVE;
      win.show_all ();
      Gtk.main ();
    });
    Test.add_func ("/gsvgtk/render/polygon",
    ()=>{
      GSvg.Document svg = new GSvg.GsDocument ();
      var s = svg.add_svg (null, null, "100mm", "100mm");
      var pg = s.create_polygon ("50,100 100,100 100,50 150,50 150,200 100,200 100,150");
      pg.id = "pòlyline";
      s.polygons.append (pg);
      var win = new Gtkt.WindowTester ();
      var w = new GtkClutter.Embed () {
        width_request = 400,
        height_request = 400 };
      w.get_stage ().width = 400;
      w.get_stage ().height = 400;
      w.get_stage ().x = 0;
      w.get_stage ().y = 0;
      win.widget = w;
      var img = new GSvgtk.ActorImageClutter ();
      img.svg = svg;
      assert (img.svg != null);
      img.renderer = new GSvgtk.SvgRendererCairo ();
      assert (img.renderer is SvgRendererCairo);
      assert (img.renderer.svg != null);
      w.get_stage ().add_child (img);
      win.add_test ("GSVGtk.Render Test","Render a Polygon");
      win.add_test ("GSVGtk.Render Test","Render a Polygon no fill");
      win.add_test ("GSVGtk.Render Test","Render a Polygon no stroke && filled");
      win.add_test ("GSVGtk.Render Test","Render a Polygon translated");
      win.initialize.connect (()=>{
        switch (win.current_ntest) {
        case 1:
          pg.stroke = "green";
          pg.stroke_width = "1mm";
          img.drawing_area.invalidate ();
        break;
        case 2:
          pg.fill = "none";
          pg.stroke = "green";
          pg.stroke_width = "1mm";
          img.drawing_area.invalidate ();
        break;
        case 3:
          pg.fill = "red";
          pg.stroke = null;
          pg.stroke_width = null;
          img.drawing_area.invalidate ();
        break;
        case 4:
          pg.transform = new GsAnimatedTransformList ();
          pg.transform.value = "translate(100 100)";
          img.drawing_area.invalidate ();
        break;
        }
      });
      win.check.connect (()=>{
      });
      win.waiting_for_event = ConfigTest.TEST_INTERACTIVE;
      win.show_all ();
      Gtk.main ();
    });
    Test.add_func ("/gsvgtk/render/container",
    ()=>{
      try {
        GSvg.Document svg = null;
        var win = new Gtkt.WindowTester ();
        var s = new GSvgtk.ActorContainerClutter ();
        var w = new GtkClutter.Embed () {
          width_request = 600,
          height_request = 600 };
        w.get_stage ().width = 600;
        w.get_stage ().height = 600;
        w.get_stage ().x = 0;
        w.get_stage ().y = 0;
        win.widget = w;
        w.get_stage ().add_child (s);
        var f = GLib.File.new_for_uri ("file://"+ConfigTest.TEST_DIR+"/shapes.svg");
        message (f.get_path ());
        assert (f.query_exists ());
        svg = new GSvg.GsDocument ();
        svg.read_from_file (f);
        var img = new GSvgtk.ActorImageClutter ();
        img.svg = svg;
        img.renderer = new SvgRendererCairo ();
        win.add_test ("Render SVG in Container","An stage with an SVG");
        win.initialize.connect (()=>{
          switch (win.current_ntest) {
          case 1:
            s.add_child (img as Clutter.Actor);
            img.drawing_area.invalidate ();
          break;
          }
        });
        win.check.connect (()=>{
        });
        win.waiting_for_event = ConfigTest.TEST_INTERACTIVE;
        win.show_all ();
        Gtk.main ();
      } catch (GLib.Error e) { warning ("Error: "+e.message); }
    });
    Test.add_func ("/gsvgtk/render/container/actors",
    ()=>{
      try {
        GSvg.Document svg = null;
        var win = new Gtkt.WindowTester ();
        var s = new GSvgtk.ActorContainerClutter ();
        var w = new GtkClutter.Embed () {
          width_request = 600,
          height_request = 600 };
        w.get_stage ().width = 600;
        w.get_stage ().height = 600;
        w.get_stage ().x = 0;
        w.get_stage ().y = 0;
        win.widget = w;
        w.get_stage ().add_child (s);
        win.add_test ("Extract Shape Container","An stage with a shapes. Put mouse over");
        var f = GLib.File.new_for_uri ("file://"+ConfigTest.TEST_DIR+"/shapes.svg");
        message (f.get_path ());
        assert (f.query_exists ());
        svg = new GSvg.GsDocument ();
        svg.read_from_file (f);
        win.initialize.connect (()=>{
          try {
            switch (win.current_ntest) {
            case 1:
              var sh1 = svg.get_element_by_id ("ellipse");
              assert (sh1 != null);
              GSvgtk.ActorEllipse e1 = new GSvgtk.ActorEllipseClutter.from_id (svg, "ellipse");
              s.add_child (e1 as Clutter.Actor);
              var sh2 = svg.get_element_by_id ("rectangle");
              assert (sh2 != null);
              var r1 = new GSvgtk.ActorRectClutter.from_id (svg, "rectangle");
              s.add_child (r1 as Clutter.Actor);
              var sh3 = svg.get_element_by_id ("circle");
              assert (sh3 != null);
              var c1 = new GSvgtk.ActorCircleClutter.from_id (svg, "circle");
              s.add_child (c1 as Clutter.Actor);
              var sh4 = svg.get_element_by_id ("line1");
              assert (sh4 != null);
              GSvgtk.ActorLine l1 = new GSvgtk.ActorLineClutter.from_id (svg, "line1");
              s.add_child (l1 as Clutter.Actor);
              var sh5 = svg.get_element_by_id ("line2");
              assert (sh5 != null);
              GSvgtk.ActorLine l2 = new GSvgtk.ActorLineClutter.from_id (svg, "line2");
              s.add_child (l2 as Clutter.Actor);
              var sh6 = svg.get_element_by_id ("line3");
              assert (sh6 != null);
              GSvgtk.ActorLine l3 = new GSvgtk.ActorLineClutter.from_id (svg, "line3");
              s.add_child (l3 as Clutter.Actor);
              var sh7 = svg.get_element_by_id ("line4");
              assert (sh7 != null);
              GSvgtk.ActorLine l4 = new GSvgtk.ActorLineClutter.from_id (svg, "line4");
              s.add_child (l4 as Clutter.Actor);
              var sh8 = svg.get_element_by_id ("text");
              assert (sh8 != null);
              var t1 = new GSvgtk.ActorTextClutter.from_id (svg, "text");
              s.add_child (t1 as Clutter.Actor);
            break;
            }
          } catch (GLib.Error e) { warning ("Error: "+e.message); }
        });
        win.check.connect (()=>{
        });
        win.waiting_for_event = ConfigTest.TEST_INTERACTIVE;
        win.show_all ();
        Gtk.main ();
      } catch (GLib.Error e) { warning ("Error: "+e.message); }
    });
    Test.add_func ("/gsvgtk/render/g",
    ()=>{
      GSvg.Document svg = new GSvg.GsDocument ();
      var s = svg.add_svg (null, null, "100mm", "100mm");
      var t = s.create_text ("Unmodified", "10mm", "20mm", null, null, null);
      t.fill = "green";
      t.font_size = "6mm";
      t.font_family = "Sans";
      t.id = "textu";
      s.texts.append (t);
      var g = s.create_g ();
      g.transform = new GsAnimatedTransformList ();
      g.transform.value = "translate(200 200)";
      g.id = "groupt";
      var t2 = s.create_text ("Modified", "10mm", "20mm", null, null, null);
      t2.fill = "blue";
      t2.font_size = "6mm";
      t2.font_family = "Sans";
      t2.id = "text";
      g.texts.append (t2);
      s.groups.append (g);
      var win = new Gtkt.WindowTester ();
      var w = new GtkClutter.Embed () {
        width_request = 400,
        height_request = 400 };
      w.get_stage ().width = 400;
      w.get_stage ().height = 400;
      w.get_stage ().x = 0;
      w.get_stage ().y = 0;
      win.widget = w;
      var img = new GSvgtk.ActorImageClutter ();
      img.svg = svg;
      assert (img.svg != null);
      img.renderer = new GSvgtk.SvgRendererCairo ();
      assert (img.renderer is SvgRendererCairo);
      assert (img.renderer.svg != null);
      w.get_stage ().add_child (img);
      win.add_test ("GSVGtk.Render Test","Render text unmodified / grouped text translated");
      win.add_test ("GSVGtk.Render Test","Render text unmodified / grouped text fill from G");
      win.add_test ("GSVGtk.Render Test","Render text unmodified / grouped text in G scaled");
      win.add_test ("GSVGtk.Render Test","Render text unmodified / grouped text in G rotated");
      win.add_test ("GSVGtk.Render Test","Render text unmodified / grouped text in G skewX(+)");
      win.add_test ("GSVGtk.Render Test","Render text unmodified / grouped text in G skewX(-)");
      win.add_test ("GSVGtk.Render Test","Render text unmodified / grouped text in G skewY(+)");
      win.add_test ("GSVGtk.Render Test","Render text unmodified / grouped text in G skewY(-)");
      win.add_test ("GSVGtk.Render Test","Render text unmodified / grouped text in G Matrix");
      win.initialize.connect (()=>{
        switch (win.current_ntest) {
        case 1:
          img.drawing_area.invalidate ();
        break;
        case 2:
          g.font_family = "Bitstream Charter";
          g.fill = "yellow";
          t2.font_family = null;
          t2.fill = null;
          img.drawing_area.invalidate ();
        break;
        case 3:
          g.transform = new GsAnimatedTransformList ();
          g.transform.value = "scale(3 5)";
          g.font_family = "Bitstream Charter";
          g.fill = "red";
          t2.font_family = null;
          t2.fill = null;
          img.drawing_area.invalidate ();
        break;
        case 4:
          g.transform = new GsAnimatedTransformList ();
          g.transform.value = "translate (50 50) rotate(%0g)".printf (Math.PI / 4);
          g.font_family = "Bitstream Charter";
          g.fill = "black";
          t2.font_family = null;
          t2.fill = null;
          img.drawing_area.invalidate ();
        break;
        case 5:
          g.transform = new GsAnimatedTransformList ();
          g.transform.value = "translate (50 50) skewX(%0g)".printf (Math.PI / 4);
          g.font_family = "Bitstream Charter";
          g.fill = "black";
          t2.font_family = null;
          t2.fill = null;
          img.drawing_area.invalidate ();
        break;
        case 6:
          g.transform = new GsAnimatedTransformList ();
          g.transform.value = "translate (50 50) skewX(%0g)".printf (-Math.PI / 4);
          g.font_family = "Bitstream Charter";
          g.fill = "black";
          t2.font_family = null;
          t2.fill = null;
          img.drawing_area.invalidate ();
        break;
        case 7:
          g.transform = new GsAnimatedTransformList ();
          g.transform.value = "translate (50 50) skewY(%0g)".printf (Math.PI / 4);
          g.font_family = "Bitstream Charter";
          g.fill = "black";
          t2.font_family = null;
          t2.fill = null;
          img.drawing_area.invalidate ();
        break;
        case 8:
          g.transform = new GsAnimatedTransformList ();
          g.transform.value = "translate (100 100) skewY(%0g)".printf (-Math.PI / 4);
          g.font_family = "Bitstream Charter";
          g.fill = "black";
          t2.font_family = null;
          t2.fill = null;
          img.drawing_area.invalidate ();
        break;
        case 9:
          g.transform = new GsAnimatedTransformList ();
          double angle = Math.PI / 3;
          g.transform.value = "translate (100 100) matrix(%0g %0g %0g %0g 100 100)".printf (Math.cos (angle), Math.sin(angle), -Math.sin(angle), Math.cos(angle));
          g.font_family = "Bitstream Charter";
          g.fill = "black";
          t2.font_family = null;
          t2.fill = null;
          img.drawing_area.invalidate ();
        break;
        }
      });
      win.check.connect (()=>{
      });
      win.waiting_for_event = ConfigTest.TEST_INTERACTIVE;
      win.show_all ();
      Gtk.main ();
    });
    return Test.run ();
  }
}
