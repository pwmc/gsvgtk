/* -*- Mode: vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*- */
/* gsvgtk-actor-image.vala
 *
 * Copyright (C) 2018 Daniel Espinosa <daniel.espinosa@pwmc.mx>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using GSvg;

public interface GSvgtk.ActorImage : GLib.Object, GSvgtk.Actor
{
    /**
    * Imports an SVG from a {@link GSvg.Document}.
    */
    public virtual bool import_svg (GSvg.DomDocument svg) throws GLib.Error
    {
        element = drawing_area.add_svg (svg);

        return true;
    }
    /**
    * Imports an SVG from a {@link GSvg.Document}.
    */
    public virtual bool import_svg_from_string (string svg) throws GLib.Error {
        var doc = new GSvg.Document ();
        doc.read_from_string (svg);
        return import_svg (doc);
    }
    /**
    * Imports an SVG from a {@link GLib.File}.
    */
    public virtual bool import_svg_from_stream (GLib.InputStream stream) throws GLib.Error {
        var doc = new GSvg.Document ();
        doc.read_from_stream (stream);
        return import_svg (doc);
    }
    /**
    * Imports an SVG from a {@link GLib.File}.
    */
    public virtual bool import_svg_from_file (GLib.File file) throws GLib.Error {
        if (!file.query_exists ()) {
            warning ("File doesn't exists: %s", file.get_uri ());
            return false;
        }

        return import_svg_from_stream (file.read ());
    }
}
