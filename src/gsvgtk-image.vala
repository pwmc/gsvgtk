/* -*- Mode: vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*- */
/* gsvgtk-actor-image-dwa.vala
 *
 * Copyright (C) 2019 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using GSvg;

public class GSvgtk.Image : GSvgtk.Item
{
  public Image (GSvgtk.DrawingArea dw, string id, Actor parent) {
    GLib.Object (id: id, parent: parent, drawing_area: dw);
    parent.add (id, this);
  }

  public override bool import_svg (GSvg.DomDocument doc) {
    if (_svg == null) {
      _svg = new GSvg.Document ();
    }
    try {
      _element = drawing_area.add_svg (doc);
      return true;
    } catch (GLib.Error e) {
      warning ("Error while importing image: %s", e.message);
    }

    return false;
  }
}
