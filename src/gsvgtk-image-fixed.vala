/* gsvgtk-image.vala
 *
 * Copyright (C) 2017 Daniel Espinosa <daniel.espinosa@pwmc.mx>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using GSvg;
using Gtk;
using GXml;

public class GSvgtk.ImageFixed : Gtk.Box {
    GSvgtk.Actor actor;
    GSvgtk.DrawingAreaGtk dwa;
    private GSvg.Document _svg = null;
    private int _width = -1;
    private int _height = -1;

    public string str {
        owned get {
            try {
                return ((GXml.Document) svg).write_string ();
            } catch (GLib.Error e) {
                warning ("Error: "+e.message);
                return "";
            }
        }
        set {
            try {
                if (svg == null) svg = new GSvg.Document ();
                ((GXml.Document) svg).read_from_string (str);
            } catch (GLib.Error e) {
                warning ("Error: "+e.message);
            }
        }
    }

    public GSvg.Document svg {
        get {
            if (_svg == null) {
                _svg = new GSvg.Document ();
            }

            return _svg;
        }
        set {
            _svg = value;
            try {
                render ();
            } catch (GLib.Error e) {
                warning ("Error: "+e.message);
            }
        }
    }
    /**
     * Width in pixels, to be used to render the SVG.
     *
     * A value of -1, means the size of the image will be rendered
     * at its own size.
     *
     * Use {@link render} in order to change the image on screen.
     */
    public int width {
        get { return _width; }
    }
    /**
     * Height in pixels, to be used to define when SVG is rendered
     *
     * A value of -1, means the size of the image will be rendered
     * at its own size.
     *
     * Use {@link render} in order to change the image on screen.
     */
    public int height {
        get { return _height; }
    }
    /**
     * Set {@link width} and {@link height} properties and calls {@link render}
     */
    public void set_size (int width, int height) throws GLib.Error {
        _width = width;
        _height = height;
        render ();
    }

    construct {
        actor = new GSvgtk.Item ();
        this.append (dwa);
        try {
            load_from_resource ("resource:///gsvg/GSVGtk/logo.svg");
        } catch (GLib.Error e) {
            warning ("Error loading default image");
        }
    }

    /**
     * Render the SVG to an image.
     */
    public void render () throws GLib.Error {
        render_internal (null);
    }
    /**
     * Add a transformation string to the image
     */
    public void add_transformation (string transform) throws GLib.Error {
        render_internal (transform);
    }
    /**
     * Add a scales the image to the given value.
     */
    public void scale (double scale) throws GLib.Error {
        var transform = "scale(%g)".printf (scale);
        render_internal (transform);
    }
    /**
     * Reads the SVG image from resource's URI of the form: {{{ resource:///path/to/your/file }}}
     */
    public void load_from_resource (string res) throws GLib.Error {
        GLib.File f = GLib.File.new_for_uri (res);
        var s = new GSvg.Document ();
        s.read_from_stream (f.read ());
        svg = s;
    }

    internal void render_internal (string? transform) throws GLib.Error {
        if (_svg == null) {
            return;
        }

        var s = new GSvg.Document ();
        var rsvg = s.root_element;
        var g = rsvg.add_g ();
        // if (width >= 0) {
        //     rsvg.width = new GSvg.AnimatedLength ();
        //     rsvg.width.base_val.value = _width;
        // }

        // if (height >= 0) {
        //     rsvg.height = new GSvg.AnimatedLength ();
        //     rsvg.height.base_val.value = _height;
        // }

        var im = s.create_svg (null, null, null, null, null, null);
        im.read_from_string (_svg.write_string ());
        g.svgs.append (im);
        if (transform != null) {
            g.transform = new AnimatedTransformList ();
            g.transform.value = transform;
        }

        actor.drawing_area.svg = s;
    }
}
