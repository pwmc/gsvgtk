/* gsvgtk-drawing-area.vala
 *
 * Copyright (C) 2017 Daniel Espinosa <daniel.espinosa@pwmc.mx>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


public interface GSvgtk.DrawingArea : Object {
    /**
    * An SVG to render in the DrawingArea
    */
    public abstract GSvg.DomDocument svg { get; internal set; }
    /**
    * An SVG to render in the DrawingArea
    */
    public abstract GSvg.DomDocument viewer { get; internal set; }
    /**
    * An SVG to render in the DrawingArea
    */
    public abstract GSvg.DomSvgElement container { get; internal set; }
    /**
     * The SVG document's URI, used to access referenced
     * images files in the same location
     */
    public abstract GLib.File? file { get; internal set; }
    /**
     * SVG renderer
     */
    public abstract GSvgtk.SvgRenderer renderer { get; set; }
    /**
     * X axis points per milimeter for current
     * display device. A -1 value means system
     * dots per milimeters are used.
     */
    public abstract double ppmm { get; }
    /**
     * Drawing width in milimeters. A -1 value means system
     * dots per milimeters are used.
     */
    public abstract double width_mm { get; set; }
    /**
     * Drawing height in milimeters. A -1 value means system
     * dots per milimeters are used.
     */
    public abstract double height_mm { get; set; }
    /**
     * Draw is always the same size
     */
    public abstract bool fixed_size { get; set; }
    /**
     * Invalidate the drawing in order to
     * redraw the SVG
     */
    public abstract void invalidate ();
    /**
     * Set the drawing's size in pixels
     */
    public abstract void set_drawing_size (int width, int  height);
    /**
     * Get current drawing's size
     */
    public abstract void get_drawing_size (out int width, out int  height);

    /**
     * Add an {@link GSvg.DomDocument}'s {@link GSvg.DomSvgElement} root
     * to the current modified SVG. This new SVG is added but the URI of
     * container's SVG document is unchanged, so resources, like images in this
     * new SVG should be located in the save URI.
     */
    public virtual GSvg.DomSvgElement add_svg (GSvg.DomDocument doc, string? uri = null) throws GLib.Error {
        if (viewer == null) {
            return replace_svg (doc, null);
        } else {
            var e = container.create_svg (null, null, null, null, null, null, null);
            e.read_from_stream (doc.create_stream ());
            container.svgs.append (e);

            var en = svg.root_element.create_svg (null, null, null, null, null, null, null);
            en.read_from_string (doc.write_string (null));
            svg.root_element.svgs.append (en);
            return en;
        }
    }

    /**
     * Replace current SVG with the given {@link GSvg.DomDocument}
     */
    public virtual GSvg.DomSvgElement replace_svg (GSvg.DomDocument doc, GLib.File? file = null)
        throws GLib.Error
    {
        //  requires (doc.root_element != null)
        svg = doc;
        this.file = file;
        viewer = new GSvg.Document ();
        container = viewer.root_element;

        var e = container.create_svg (null, null, null, null, null, null, null);
        e.read_from_stream (doc.root_element.create_stream ());
        container.svgs.append (e);
        invalidate ();
        return e;
    }

    /**
     * Execute a rendering of current SVG using {@link GSvgtk.SvgRenderer}
     * to a drawing area
     */
    public virtual bool draw_svg (GSvgtk.Context ctx, double w, double h, GLib.Cancellable? cancellable = null)
        requires (viewer != null)
        requires (svg != null)
    {
        if (fixed_size && width_mm > 0 && height_mm > 0) {
            try {
                renderer.ppmm = ppmm;
                renderer.width = width_mm;
                renderer.height = height_mm;
                debug ("SVG FIXED SIZE to Render:\n%s", viewer.write_string ());
                renderer.render (ctx, viewer, file, cancellable);
                return true;
            } catch (GLib.Error e) {
                warning ("Error drawing SVG: %s", e.message);
            }
        } else {
            try {
                if (svg == null) {
                    return false;
                }

                debug ("SVG NON FIXED SIZE to Render:\n%s", viewer.write_string ());
                renderer.ppmm = ppmm;
                renderer.render_sized (ctx, w, h, svg, file, cancellable);
            } catch (GLib.Error e) {
                warning ("Error drawing SVG: %s", e.message);
            }
        }

        return true;
    }

    /**
     * Use given SVG as a source and extract a {@link GSvg.Element},
     * with give id, to draw it.
     */
    public virtual GSvg.DomElement? extract_element_from_id (GSvg.DomDocument svg, string id) {
        try {
            var e = svg.get_element_by_id (id) as GSvg.Element;
            if (e == null) {
              warning ("Requested shape ID '%s' not found in SVG document".printf (id));
                return null;
            }

            return e;
        } catch (GLib.Error e) {
            warning ("Error: %s".printf (e.message));
        }

        return null;
    }
}
