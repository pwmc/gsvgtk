/* -*- Mode: vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*- */
/* gsvgtk-editor.vala
 *
 * Copyright (C) 2018 Daniel Espinosa <daniel.espinosa@pwmc.mx>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using GSvg;

public interface GSvgtk.ActorEditor : GLib.Object {
  /**
   * A collection of changes, in order to provide undo capabilities.
   */
  public abstract GSvgtk.Changes changes { get; internal set; }
  public abstract GSvgtk.Actor actor  { get; internal set; }
  /**
   * Relative point list, using negative values when is moved in values with
   * lower value of the origin and positive if the are ahead.
   */
  public abstract GSvg.DomPointList movedto { get; internal set; }
  public abstract GSvg.DomSvgElement modifications { get; internal set; }
  public abstract GSvg.DomSvgElement selection_box { get; internal set; }
  public abstract GSvg.DomElement selected { get; internal set; }

  public virtual void save () {
    try {
      if (actor is ActorShape) {
        var shape = (ActorShape) actor;
        if (shape.element is GSvg.Element) {
          ActorShape.copy_attributes_element (modifications, shape.element);
        }
        if (shape.element is GSvg.DomTransformable && modifications is GSvg.DomTransformable) {
          if (((DomTransformable) modifications).transform != null) {
            var tr = (DomTransformable) shape.element;
            tr.transform = new AnimatedTransformList ();
            tr.transform.value = ((DomTransformable) modifications).transform.value;
          }
        }
        if (movedto == null) return;
        if (shape.element is DomCircleElement) {
          if (movedto.number_of_items == 0) return;
          var p = movedto.get_item (0);
          var c = (DomCircleElement) shape.element;
          c.cx = new AnimatedLengthCX ();
          c.cx.base_val.value = p.x;
          c.cy = new AnimatedLengthCY ();
          c.cy.base_val.value = p.y;
        }
        if (shape.element is DomEllipseElement) {
          if (movedto.number_of_items == 0) return;
          var p = movedto.get_item (0);
          var e = (DomEllipseElement) shape.element;
          e.cx = new AnimatedLengthCX ();
          e.cx.base_val.value = p.x;
          e.cy = new AnimatedLengthCY ();
          e.cy.base_val.value = p.y;
        }
        if (shape.element is DomTextElement) {
          var t = (DomTextElement) shape.element;
          if (t.x != null) {
            for (int i = 0; i < t.x.base_val.number_of_items; i++) {
              var x = t.x.base_val.get_item (i);
              if (i < movedto.number_of_items) {
                var p = movedto.get_item (i);
                x.value += p.x;
              }
            }
          }
          if (t.y != null) {
            for (int i = 0; i < t.y.base_val.number_of_items; i++) {
              var y = t.y.base_val.get_item (i);
              if (i < movedto.number_of_items) {
                var p = movedto.get_item (i);
                y.value += p.y;
              }
            }
          }
        }
        if (shape.element is DomLineElement) {
          var l = (DomLineElement) shape.element;
          if (l.x1 != null && l.y1 != null) {
            if (movedto.number_of_items > 0) {
              var p = movedto.get_item (0);
              l.x1.base_val.value += p.x;
              l.y1.base_val.value += p.y;
            }
          }
          if (l.x2 != null && l.y2 != null) {
            if (movedto.number_of_items > 1) {
              var p = movedto.get_item (1);
              l.x2.base_val.value += p.x;
              l.y2.base_val.value += p.y;
            }
          }
        }
        if (shape.element is DomRectElement) {
          var r = (RectElement) shape.element;
          if (movedto.number_of_items > 0) {
            var p = movedto.get_item (0);
            r.x.base_val.value += p.x;
            r.y.base_val.value += p.y;
          }
        }
      }
    } catch (GLib.Error e) { warning ("Error: %s".printf (e.message)); }
  }
  public virtual void add_transformations (string transform) {
    if (((DomTransformable) modifications).transform == null) ((DomTransformable) modifications).transform = new AnimatedTransformList ();
    try {
      var t = new AnimatedTransformList ();
      t.value = transform;
      for (int i = 0; i < t.base_val.number_of_items; i++) {
        var tr = t.base_val.get_item (i);
        ((DomTransformable) modifications).transform.base_val.append_item (tr);
      }
    } catch (GLib.Error e) { warning ("Error: %s".printf (e.message)); }
  }
  public virtual void set_css (string css) {
    var s = (DomStylable) modifications;
    s.style = new GSvg.CssStyleDeclaration ();
    s.style.value = css;
  }
  public virtual GSvg.DomPoint current_point () {
    GSvg.Point p = new GSvg.Point ();
    try {
      if (actor is ActorShape)  {
        var shape = (ActorShape) actor;
        if (shape.element is CircleElement)  {
          var c = (DomCircleElement) shape.element;
          p.x = c.cx.base_val.value;
          p.y = c.cy.base_val.value;
        }
        if (shape.element is DomEllipseElement)  {
          var e = (DomEllipseElement) shape.element;
          p.x = e.cx.base_val.value;
          p.y = e.cy.base_val.value;
        }
        if (shape.element is DomTextElement)  {
          var t = (TextElement) shape.element;
          if (t.x == null || t.y == null) return p;
          if (t.x.base_val.number_of_items > 0) {
            p.x = t.x.base_val.get_item (0).value;
          }
          if (t.y.base_val.number_of_items > 0) {
            p.y = t.y.base_val.get_item (0).value;
          }
        }
        if (shape.element is DomRectElement)  {
          var r = (DomRectElement) shape.element;
          if (r.x == null || r.y == null) return p;
          p.x = r.x.base_val.value;
          p.y = r.y.base_val.value;
        }
      }
    } catch (GLib.Error e) { warning ("Error: %s".printf (e.message)); }
    return p;
  }
  public abstract void undo ();
  public virtual void select (GSvg.DomElement? e) throws GLib.Error {
    debug ("Selecting");
    if (selection_box != null) {
      selection_box.remove ();
      selection_box = null;
    }
    if (e == null || (selected != null && e == selected)) {
      debug ("No Selection");
      return;
    }

    selection_box = modifications.add_svg ("0", "0", "0", "0", null, null, null);
    selection_box.id = "#gsvgkt@selection_box@";
    debug ("empty box: %s", modifications.write_string ());

    var r1 = selection_box.create_rect ("0","0","20","20",null, null, "fill: #EAA10F");
    var r2 = selection_box.create_rect ("45%","0","20","20",null, null, "fill: #EAA10F");
    var r3 = selection_box.create_rect ("90%","0","20","20",null, null, "fill: #EAA10F");
    var r4 = selection_box.create_rect ("90%","45%","20","20",null, null, "fill: #EAA10F");
    var r5 = selection_box.create_rect ("90%","90%","20","20",null, null, "fill: #EAA10F");
    var r6 = selection_box.create_rect ("45%","90%","20","20",null, null, "fill: #EAA10F");
    var r7 = selection_box.create_rect ("0","90%","20","20",null, null, "fill: #EAA10F");
    var r8 = selection_box.create_rect ("0","45%","20","20",null, null, "fill: #EAA10F");
    var c1 = selection_box.create_circle ("50%","50%", "20", "fill: #EAA10F");
    if (e is DomCircleElement) {
      debug ("Selecting Circle");
      var el = e as CircleElement;
      var nel = selection_box.create_circle (null, null, null, null);
      ((GXml.Element) nel).read_from_string (((GXml.Element) el).write_string ());
      selection_box.append_child (nel);
      double r = modifications.convert_length_x_pixels (el.r.base_val);
      selection_box.x.base_val.value = modifications.convert_length_x_pixels (el.cx.base_val) - r*1.2;
      selection_box.y.base_val.value = modifications.convert_length_x_pixels (el.cy.base_val) - r*1.2;
      selection_box.width.base_val.value = r*2*1.2;
      selection_box.height.base_val.value = r*2*1.2;
      nel.cx.base_val.value = el.r.base_val.value * 1.2;
      nel.cy.base_val.value = el.r.base_val.value * 1.2;
    } else if (e is EllipseElement) {
      var el = e as EllipseElement;
      var nel = selection_box.create_ellipse (null, null, null, null, null);
      ((GXml.Element) nel).read_from_string (((GXml.Element) el).write_string ());
      selection_box.append_child (nel);
      double rx = modifications.convert_length_x_pixels (el.rx.base_val);
      double ry = modifications.convert_length_x_pixels (el.ry.base_val);
      selection_box.x.base_val.value = modifications.convert_length_x_pixels (el.cx.base_val) - rx*1.2;
      selection_box.y.base_val.value = modifications.convert_length_x_pixels (el.cy.base_val) - ry*1.2;
      selection_box.width.base_val.value = rx*2*1.2;
      selection_box.height.base_val.value = ry*2*1.2;
      nel.cx.base_val.value = el.rx.base_val.value * 1.2;
      nel.cy.base_val.value = el.ry.base_val.value * 1.2;
    } else if (e is RectElement) {
      var el = e as RectElement;
      var nel = selection_box.create_rect (null, null, null, null, null, null, null);
      ((GXml.Element) nel).read_from_string (((GXml.Element) el).write_string ());
      selection_box.append_child (nel);
      double width = (int) modifications.convert_length_x_pixels (el.width.base_val);
      double height = (int) modifications.convert_length_y_pixels (el.height.base_val);
      double x = (int) modifications.convert_length_x_pixels (el.x.base_val);
      double y = (int) modifications.convert_length_y_pixels (el.y.base_val);
      selection_box.x.base_val.value = x - 20;
      selection_box.y.base_val.value = y - 20;
      selection_box.width.base_val.value = width + 40;
      selection_box.height.base_val.value = height + 40;
      nel.x.base_val.value = 20 * el.x.base_val.value / x;
      nel.y.base_val.value = 20 * el.y.base_val.value / y;
    } else if (e is LineElement) {
      var el = e as LineElement;
      var nel = selection_box.create_line (null, null, null, null, null);
      ((GXml.Element) nel).read_from_string (((GXml.Element) el).write_string ());
      selection_box.append_child (nel);
      double x1 = (int) modifications.convert_length_x_pixels (el.x1.base_val);
      double y1 = (int) modifications.convert_length_y_pixels (el.y1.base_val);
      double x2 = (int) modifications.convert_length_x_pixels (el.x2.base_val);
      double y2 = (int) modifications.convert_length_y_pixels (el.y2.base_val);
      selection_box.width.base_val.value = (x2 - x1).abs ();
      selection_box.height.base_val.value = (y2 - y1).abs ();
      double xd, yd;
      xd = yd = 20.0;
      if (selection_box.width.base_val.value < 62.0) {
        selection_box.width.base_val.value = 62.0;
        xd = 31.0;
      } else {
        selection_box.width.base_val.value += 40.0;
      }
      if (selection_box.height.base_val.value < 62.0) {
        selection_box.height.base_val.value = 62.0;
        yd = 31.0;
      } else {
        selection_box.height.base_val.value += 40.0;
      }
      double x, y;
      if (x2 > x1) {
        x = x1;
      } else {
        x = x2;
      }
      if (y2 > y1) {
        y = y1;
      } else {
        y = y2;
      }
      selection_box.x.base_val.value = x - xd;
      selection_box.y.base_val.value = y - yd;
      nel.x1.base_val.value = 20 * el.x1.base_val.value / x1;
      nel.y1.base_val.value = 20 * el.y1.base_val.value / y1;
      nel.x2.base_val.value = 20 * el.x2.base_val.value / x2;
      nel.y2.base_val.value = 20 * el.y2.base_val.value / y2;
    } else {
      selection_box.width.base_val.value = modifications.width.base_val.value;
      selection_box.height.base_val.value = modifications.height.base_val.value;
    }
    selection_box.append_child (r1);
    r2.x.base_val.value = selection_box.width.base_val.value / 2 - 10;
    r2.x.base_val.unit_type = DomLength.Type.PX;
    selection_box.append_child (r2);
    selection_box.append_child (r3);
    r3.x.base_val.value = selection_box.width.base_val.value - 20;
    r3.x.base_val.unit_type = DomLength.Type.PX;
    selection_box.append_child (r4);
    r4.y.base_val.value = selection_box.height.base_val.value / 2 - 10;
    r4.y.base_val.unit_type = DomLength.Type.PX;
    r4.x.base_val.value = selection_box.width.base_val.value - 20;
    r4.x.base_val.unit_type = DomLength.Type.PX;
    selection_box.append_child (r5);
    r5.x.base_val.value = selection_box.width.base_val.value - 20;
    r5.x.base_val.unit_type = DomLength.Type.PX;
    r5.y.base_val.value = selection_box.height.base_val.value - 20;
    r5.y.base_val.unit_type = DomLength.Type.PX;
    selection_box.append_child (r6);
    r6.x.base_val.value = selection_box.width.base_val.value / 2 - 10;
    r6.x.base_val.unit_type = DomLength.Type.PX;
    r6.y.base_val.value = selection_box.height.base_val.value - 20;
    r6.y.base_val.unit_type = DomLength.Type.PX;
    selection_box.append_child (r7);
    r7.y.base_val.value = selection_box.height.base_val.value - 20;
    r7.y.base_val.unit_type = DomLength.Type.PX;
    selection_box.append_child (r8);
    r8.y.base_val.value = selection_box.height.base_val.value / 2 - 10;
    r8.y.base_val.unit_type = DomLength.Type.PX;
    selection_box.append_child (c1);
    message (modifications.write_string ());
    actor.drawing_area.invalidate ();
  }
}
