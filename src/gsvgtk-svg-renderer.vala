/* -*- Mode: vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*- */
/* gsvgtk-svg-renderer.vala
 *
 * Copyright (C) 2018 Daniel Espinosa <daniel.espinosa@pwmc.mx>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using GSvg;

public interface GSvgtk.SvgRenderer : GLib.Object {
  public abstract string id { get; set; }
  /**
   * Points per milimeter on X axis.
   */
  public abstract double ppmm { get; set; }
  /**
   * Width in milimeters
   */
  public abstract double width { get; set; }
  /**
   * Height in milimeters
   */
  public abstract double height { get; set; }
  /**
   * Render on given {@link GSvgtk.Context}
   *
   * If {@link id} has been set, this method render just
   * the {@link GXml.DomElement} with that id.
   *
   * @param ctx a context to render to
   */
  public abstract void render (GSvgtk.Context ctx, GSvg.DomDocument svg,
                                                         GLib.File? file = null, GLib.Cancellable? cancellable = null)
                                                         throws GLib.Error;
  /**
   * Render on given {@link GSvgtk.Context}.
   *
   * If {@link id} has been set, this method render just
   * the {@link GXml.DomElement} with that id.
   *
   * @param ctx a context to render to
   * @param width in milimeters
   * @param height in milimeters
   */
  public abstract void render_sized (GSvgtk.Context ctx,
                                                                    double width, double height,
                                                                    GSvg.DomDocument svg,
                                                                    GLib.File? file = null,
                                                                    GLib.Cancellable? cancellable = null )
                                                                    throws GLib.Error;
  // Static
  /**
   * Parse an RGBA from string to its representation values
   */
  public static void parse_color_rgba (string c,
                                out double red,
                                out double gree,
                                out double blue,
                                out double alpha)
                                throws GLib.Error
  {
    if (c.down () == "none") {
      red = 0.0;
      gree = 0.0;
      blue = 0.0;
      alpha = 0.0;
      return;
    }
    string cl = c;
    string salpha = null;
    message ("Using Gdk: %s", c);
    if ("#" in cl) {
      if (cl.length == 9) {
        cl = cl.substring (0,7);
        message ("%s", cl);
        salpha = cl.substring (5);
        message ("%s", salpha);
      }
    }
    Gdk.RGBA color = Gdk.RGBA ();
    if (!color.parse (cl)) {
      warning ("Invalid color: %s", c);
    }
    red = color.red;
    gree = color.green;
    blue = color.blue;
    alpha = color.alpha;
    if (salpha != null) {
      alpha = (hex_to_int (salpha[0]) * 16 + hex_to_int (salpha[1]))/255.0;
    }
  }
  private static int hex_to_int (char c) {
    switch (c) {
      case 'a':
        return 10;
      case 'b':
        return 11;
      case 'c':
        return 12;
      case 'd':
        return 13;
      case 'e':
        return 14;
      case 'f':
        return 15;
      default:
        return int.parse ((string) c);
    }
  }
}
