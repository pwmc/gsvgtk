/* gsvgtk-svg-renderer-librsvg.vala
 *
 * Copyright (C) 2018-2024 Daniel Espinosa <daniel.espinosa@pwmc.mx>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using Rsvg;

public class GSvgtk.SvgRendererRsvg : Object, SvgRenderer {
    internal string id { get; set; }
    internal double ppmm { get; set; }
    internal double width { get; set; }
    internal double height { get; set; }
    construct {
        ppmm = 4.0;
        width = 25.0;
        height = 25.0;
    }
    internal void render (GSvgtk.Context ctx, GSvg.DomDocument svg, GLib.File? file = null, GLib.Cancellable? cancellable = null) throws GLib.Error
    requires (ctx is GSvgtk.ContextCairo)
    {
        var rsvg = new Rsvg.Handle.from_stream_sync (svg.create_stream (), file, Rsvg.HandleFlags.FLAGS_NONE, cancellable);
        rsvg.set_dpi (ppmm*25.4);
        Rsvg.Rectangle viewport = Rsvg.Rectangle ();
        viewport.width = width * ppmm;
        viewport.height = height * ppmm;
        viewport.x = 0.0;
        viewport.y = 0.0;
        if (id != "" && id != null) {
            var e = svg.get_element_by_id (id);
            if (e != null) {
                rsvg.render_element (((GSvgtk.ContextCairo) ctx).get_cairo_context (), id, viewport);
                return;
            }
        }

        rsvg.render_document (((GSvgtk.ContextCairo) ctx).get_cairo_context (), viewport);
    }
    internal void render_sized (GSvgtk.Context ctx,  double width, double height, GSvg.DomDocument svg, GLib.File? file = null, GLib.Cancellable? cancellable = null ) throws GLib.Error
        requires (ctx is GSvgtk.ContextCairo)
    {
        if (svg == null) return;
        var rsvg = new Rsvg.Handle.from_stream_sync (svg.create_stream (), file, Rsvg.HandleFlags.FLAGS_NONE, cancellable);
        double xppi = ppmm * 25.4;
        Rsvg.Rectangle viewport = Rsvg.Rectangle ();
        viewport.width = width;
        viewport.height = height;
        viewport.x = 0.0;
        viewport.y = 0.0;

        rsvg.set_dpi (xppi);

        if (id != "" && id != null) {
            var e = svg.get_element_by_id (id);
            if (e != null) {
                rsvg.render_element (((GSvgtk.ContextCairo) ctx).get_cairo_context (), id, viewport);
                return;
            }
        }

        rsvg.render_document (((GSvgtk.ContextCairo) ctx).get_cairo_context (), viewport);
    }
}
