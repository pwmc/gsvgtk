/* -*- Mode: vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*- */
/* gsvgtk-actor-container-dwa-gtk4.vala
 *
 * Copyright (C) 2019 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

public class GSvgtk.Container : Gtk.Box, GSvgtk.ActorContainer {
  private GSvgtk.Actor _position_label;
  private GSvgtk.Actor _selected;
  private bool         _dragging;
  private double       _x;
  private double       _y;

  construct {
  }

  // GSvgtk.ActorContainer
  public GSvgtk.Actor position_label { get { return _position_label; } }
  public GSvgtk.Actor selected { get { return _selected; } }
  public bool         dragging { get { return _dragging; } }
  public new void add (GSvgtk.Actor actor)
    requires (actor is GSvgtk.Item)
    requires (actor.drawing_area != null)
    requires (actor.drawing_area is Gtk.Widget)
  {
    GSvgtk.DrawingAreaGtk dwa = actor.drawing_area as GSvgtk.DrawingAreaGtk;
    append (dwa as Gtk.Widget);
    dwa.drag_gesture.drag_begin.connect ((x,y)=>{
      _dragging = true;
      _x = x;
      _y = y;
      warning ("Drag Begin: %g,%g", x, y);
    });
    dwa.drag_gesture.drag_update.connect ((dx,dy)=>{
      warning ("Drag Update: %g,%g", dx, dy);
      //layout.move (dwa, (int) (_x + dx), (int) (_y + dy));
    });
    dwa.drag_gesture.drag_end.connect ((dx,dy)=>{
      warning ("Drag end: %g,%g", dx, dy);
      //layout.move (dwa, (int) (_x + dx), (int) (_y + dy));
      _dragging = false;
      _x = -1;
      _y = -1;
    });
  }
  public void move    (GSvgtk.Actor actor, double dx, double dy) {}
  public void move_to (GSvgtk.Actor actor, double x, double y) {}
}
