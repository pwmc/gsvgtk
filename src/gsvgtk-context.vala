/* -*- Mode: vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*- */
/* gsvgtk-actor-shape-clutter.vala
 *
 * Copyright (C) 2018 Daniel Espinosa <daniel.espinosa@pwmc.mx>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using Pango;

public interface GSvgtk.Context : Object {
  public abstract GSvgtk.Surface surface { get; }
  public abstract void save ();
  public abstract void restore ();
  public abstract void translate (double x, double y);
  public abstract void scale (double x, double y);
  public abstract void rotate (double angle);
  public abstract void transform (GSvg.DomMatrix m);
  public abstract void set_source_rgba (double red, double green, double blue, double alpha);
  public abstract void set_line_width (double w);
  public abstract void move_to (double x, double y);
  public abstract void line_to (double x, double y);
  public abstract void stroke ();
  public abstract void new_sub_path ();
  public abstract void arc (double x, double y, double r, double angle1, double angle2);
  public abstract void curve_to (double x1, double y1, double x2, double y2, double x3, double y3);
  public abstract void close_path ();
  public abstract void fill ();
  public abstract void rectangle (double x, double y, double width, double height);
  public abstract Pango.Layout create_layout ();
  public abstract void update_layout (Pango.Layout pl);
  public abstract void show_layout (Pango.Layout pl);
  public abstract void layout_path (Pango.Layout pl);
}
