/* main.vala
 *
 * Copyright (C) 2019 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

public class Window : Gtk.ApplicationWindow {
  private GSvgtk.DrawingArea dwa;
  construct {
    title = "GSvgtk GTK4 Demo";
    set_default_size (800, 800);

    var c = new GSvgtk.Container ();
    var sw = new Gtk.ScrolledWindow ();
    sw.set_propagate_natural_width (true);
    sw.set_propagate_natural_height (true);
    sw.hexpand = true;
    sw.vexpand = true;
    dwa = new GSvgtk.DrawingAreaGtk ();
    //dwa.width_mm = 300;
    //dwa.height_mm = 300;
    Gtk.Box box = new Gtk.Box (Gtk.Orientation.VERTICAL, 6);
    sw.child = dwa as Gtk.Widget;
    child  = box;
    box.append (sw);
    try {
      var f = GLib.File.new_for_uri ("resource:///gsvg/GSVGtk/logo.svg");
      var d = new GSvg.Document ();
      d.read_from_file (f);
      dwa.svg = d;
    } catch (GLib.Error e) {
      warning ("Error: %s", e.message);
    }
    var bbox = new Gtk.Box (Gtk.Orientation.HORIZONTAL, 6);
    box.append (bbox);
    var buttonc = new Gtk.Button.with_label ("Add Circle");
    bbox.append (buttonc);
    buttonc.clicked.connect (()=>{
      try {
          var cr = dwa.viewer.root_element.create_circle ("53mm","53mm","50mm", null);
          cr.stroke = "red";
          cr.stroke_width = "3mm";
          cr.fill = "none";
          dwa.viewer.root_element.append_child (cr);
          message ("%s", dwa.viewer.write_string ());
          dwa.invalidate ();
      } catch (GLib.Error e) {
          message ("Error adding circle: %s", e.message);
      }
    });
    var buttonr = new Gtk.Button.with_label ("Add Rectangle");
    bbox.append (buttonr);
    buttonr.clicked.connect (()=>{
      try {
          var r = dwa.viewer.root_element.create_rect ("10mm","10mm","40mm", "40mm", null, null, null);
          r.stroke = "black";
          r.stroke_width = "3mm";
          r.fill = "none";
          dwa.viewer.root_element.append_child (r);
          message ("%s", dwa.viewer.write_string ());
          dwa.invalidate ();
      } catch (GLib.Error e) {
          message ("Error adding circle: %s", e.message);
      }
    });
    var buttonsvg = new Gtk.Button.with_label ("Open SVG");
    bbox.append (buttonsvg);
    buttonsvg.clicked.connect (()=>{
      try {
          var fs = new Gtk.FileDialog ();
          fs.accept_label = "Open";
          fs.modal = true;
          var ff = new Gtk.FileFilter ();
          ff.add_mime_type ("image/svg+xml");
          ff.add_mime_type ("image/svg");
          fs.default_filter = ff;
          fs.open.begin (this, null, (obj,res)=>{
                try {
                    var svg = new GSvg.Document ();
                    GLib.File f = fs.open.end (res);
                    svg.read_from_file (f);
                    dwa.replace_svg (svg);
                    dwa.invalidate ();
                } catch (GLib.Error e) {
                    var msg = new Gtk.AlertDialog ("Error opening file: %s", e.message);
                    msg.show (this);
                }
          });
      } catch (GLib.Error e) {
          message ("Error adding circle: %s", e.message);
      }
    });
    var buttonisvg = new Gtk.Button.with_label ("Insert SVG");
    bbox.append (buttonisvg);
    buttonisvg.clicked.connect (()=>{
      try {
          var fs = new Gtk.FileDialog ();
          var ff = new Gtk.FileFilter ();
          ff.add_mime_type ("image/svg+xml");
          ff.add_mime_type ("image/svg");
          fs.default_filter = ff;
          fs.open.begin (this, null, (obj, res)=>{
                try {
                    var svg = new GSvg.Document ();
                    GLib.File f = fs.open.end (res);
                    svg.read_from_file (f);
                    dwa.add_svg (svg);
                    dwa.invalidate ();
                } catch (GLib.Error e) {
                    var msg = new Gtk.AlertDialog ("Error: %s", e.message);
                    msg.show (this);
                }
          });
      } catch (GLib.Error e) {
          message ("Error adding circle: %s", e.message);
      }
    });
  }
  public Window (Gtk.Application app) {
    Object (application: app);
  }
}
